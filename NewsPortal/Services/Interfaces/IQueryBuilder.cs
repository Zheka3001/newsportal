﻿using System;

namespace NewsPortal.Services.Interfaces
{
	public interface IQueryBuilder
	{
		IQueryBuilder AddQuery(string query);

		IQueryBuilder AddFromDate(DateTime fromDate);

		IQueryBuilder AddParameter(string key, string value);

		IQueryBuilder RemoveParameter(string key);

		string GetQueryString();
	}
}
