﻿using Microsoft.Extensions.Configuration;
using NewsPortal.Models;
using NewsPortal.Services.Interfaces;
using System;
using System.Linq;
using System.Text;

namespace NewsPortal.Services.Implementations
{
	public class NewsApiQueryBuilder : IQueryBuilder
	{
		private readonly Query query;

		NewsApiQueryBuilder(IConfiguration configuration)
		{
			query = new Query(configuration["StartingQueryString"]);
			query.Parameters.Add(new UrlParameter("API_KEY", configuration["NewsAPIKey"]));
		}

		public static IQueryBuilder GetBuilder(IConfiguration configuration)
		{
			return new NewsApiQueryBuilder(configuration);
		}

		public IQueryBuilder AddFromDate(DateTime fromDate)
		{
			throw new NotImplementedException();
		}

		public IQueryBuilder AddQuery(string query)
		{
			this.query.QueryString = query;
			return this;
		}

		public IQueryBuilder AddParameter(string key, string value)
		{
			// Add only unique parameters
			if (!this.query.Parameters.Select(p => p.Key).Contains(key))
			{
				this.query.Parameters.Add(new UrlParameter(key, value));
			}
			else
			{
				this.query.Parameters.Find(p => p.Key == key).Value = value;
			}
			
			return this;
		}

		public IQueryBuilder RemoveParameter(string key)
		{
			var parameterToDelete = this.query.Parameters.FirstOrDefault(p => p.Key == key);

			if (parameterToDelete != null)
			{
				this.query.Parameters.Remove(parameterToDelete);
			}

			return this;
		}

		public string GetQueryString()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append(query.StartingString);
			builder.Append(query.QueryString);

			string parameters = "";

			if (query.Parameters.Count > 1)
			{
				parameters = string.Join("&", query.Parameters.Select(p => $"{p.Key}={p.Value}"));
			}
			else
			{
				parameters = query.Parameters.Select((p => $"{p.Key}={p.Value}")).FirstOrDefault();
			}

			parameters = $"?{parameters}";
			builder.Append(parameters);

			return builder.ToString();
		}
	}
}
