﻿using System.Collections.Generic;

namespace NewsPortal.Models
{
	public class Query
	{
		public string StartingString { get; } = "";

		public string QueryString { get; set; } = "";

		public List<UrlParameter> Parameters { get; set; } = new List<UrlParameter>();

		public Query(string startingString)
		{
			this.StartingString = startingString;
		}
	}
}
