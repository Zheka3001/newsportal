﻿namespace NewsPortal.Models
{
	public class UrlParameter
	{
		public UrlParameter(string key, string value)
		{
			this.Key = key;
			this.Value = value;
		}

		public string Key { get; }
		public string Value { get; set; }
	}
}
