﻿namespace NewsPortal.Models
{
    public class News
    {
        public long NewsId { get; set; }

        public string SourceName { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public string UrlToImage { get; set; }

        public string PublishedAt { get; set; }

        public string Content { get; set; }
    }
}
