﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace NewsPortal.Models
{
	public class SeedData
	{
		public static void EnsureMigrated(IApplicationBuilder app)
		{
			NewsDbContext context = app.ApplicationServices
				.CreateScope().ServiceProvider.GetRequiredService<NewsDbContext>();

			if (context.Database.GetPendingMigrations().Any())
			{
				context.Database.Migrate();
			}

			context.SaveChanges();
		}
	}
}
