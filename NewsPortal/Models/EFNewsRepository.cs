﻿using System.Linq;

namespace NewsPortal.Models
{
	public class EFNewsRepository : INewsRepository
	{
		private NewsDbContext context;

		public EFNewsRepository(NewsDbContext context)
		{
			this.context = context;
		}

		public IQueryable<News> News => context.News;
	}
}
