﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NewsPortal.Models;

namespace NewsPortal.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly INewsRepository repository;

        public HomeController(IConfiguration config, INewsRepository repository)
		{
            this.configuration = config;
            this.repository = repository;
		}

        public IActionResult Index()
        {
            return View();
        }
    }
}
